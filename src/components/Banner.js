// //import Row from 'react-bootstrap/Row';
// //import Col from 'react-bootstrap/Col';

// import { Row, Col, Button } from "react-bootstrap";

// export default function Banner(){

// 	return(

// 		<Row>
// 			<Col>
// 				<h1>B248 Booking App</h1>
// 				<p>Opportunities for everyone, everywhere!</p>
// 				<Button variant="primary">Enroll Now!</Button>
// 			</Col>
// 		</Row>
// 		)
// }

// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import { Row, Col, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';

export default function Banner({data}){

	console.log(data);
	const { title, content, destination, label } = data;
	return(

		<Row>
			<Col>
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="primary" as={Link} to={destination}>{label}</Button>
			</Col>
		</Row>

	)


}

/*
s53 Activity

1. Create a route which will be accessed when a user enters an undefined route and display the Error page.

*** When the user goes to a route that is not included in the Routes component, it should display the NotFound page component.

Stretch Goal:

- Refactor the Banner component to be useable for the Error page and the Home page.
- Push with the commit message of Add activity code - s53
- Add the link in Boodle.


*/